package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;

import java.util.List;

public interface OverflowListGoodsDao {

    // 保存商品报溢单信息
    void saveOverflowList(OverflowList overflowList);

    // 调用dao层保存   商品报溢单商品列表
    void saveOverflowListGoods(OverflowListGoods overflowListGoods);

    //根据开始时间和结束时间 查询商品报溢单
    List<OverflowList> getOverflowlist(String sTime, String eTime);

    // 根据商品报溢单ID查询商品报溢单商品信息
    List<OverflowListGoods> getOverflowListGoodsByOverflowListId(Integer overflowListId);
}
