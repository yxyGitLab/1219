package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;


import java.util.List;

public interface CustomerDao {

    //进行新增客户
    void saveCustomer(Customer customer);

    //调用持久层 按遍历的id进行 循环删除客户
    void deleteCustomer(int parseInt);

    //进行修改客户
    void updateCustomer(Customer customer);

    //通过前端传过来的名称进行模糊查询
    List<Customer> getCustomerList(int offSet, Integer pageRow, String customerName);

    //根据客户名称模糊查询出一共多少条记录
    Integer getCustomerCount(String customerName);

    //通过CustomerById 查询出相关id客户的名称
    Customer getCustomerById(int parseInt);

}
