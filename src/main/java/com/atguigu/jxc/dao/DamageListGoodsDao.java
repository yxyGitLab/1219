package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;

import java.util.List;

public interface DamageListGoodsDao {


    // 保存商品报损单信息
    void saveDamageList(DamageList damageList);

    //保存报损单信息到商品报损单商品列表
    void saveDamageListGoods(DamageListGoods damageListGoods);

    //通过开始时间和结束时间到dao层查询 商品报损单
    List<DamageList> getDamagelist(String sTime, String eTime);

    //根据 商品保存单ID 查询商品报损单商品信息
    List<DamageListGoods> getDamageListGoodsByDamageListId(Integer damageListId);

}
