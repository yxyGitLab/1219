package com.atguigu.jxc.dao;


import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Supplier;

import java.util.List;

public interface SupplierDao {
    //调用持久层进行新增
    void saveSupplier(Supplier supplier);

    //调用持久层进行批量删除
    void deleteSupplier(int parseInt);

    //调用持久层进行修改
    void updateSupplier(Supplier supplier);

    /**
     * 分页查询供应商管理列表
     * @param offSet 从多少页开始到...
     * @param rows 每页显示的记录数
     * @param supplierName 供应商名
     * @return
     */
    List<Supplier> getSupplierList(int offSet, Integer pageRow, String supplierName);

    //根据供应商名称查到数据的总记录数 一共多少条
    Integer getSupplierCount(String supplierName);

    //通过id查询到一条供应商对象
    Supplier getSupplierById(int parseInt);
}
