package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;

import java.util.List;

/**
 * @description 商品类别
 */
public interface GoodsTypeDao {

    //调用持久层进行新增
    void saveGoodsType(GoodsType goodsType);

    //根据 pid 查询回去 pid相同的 商品类别集合回去
    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    List<GoodsType> getGoodsTypeState(Integer goodTypeId);

    //状态为0表示其为叶子节点， 1为根节点，-1为顶级根节点即所有类别
    //修改状态值后进行父类节点状态修改 把修改好的商品类型对象传入dao层进行修改参数
    Integer updateGoodsTypeState(GoodsType parentGoodsType);

    /**
     * 根据 goodsTypeId删除 分类
     * @param goodsTypeId
     */
    void delete(Integer goodsTypeId);

    // 根据父类ID来查询父类实体
    GoodsType getGoodsTypeById(Integer pId);

    // 根据商品类别ID来查询商品信息，如果该类别下有商品信息，则不给予删除
    List<Goods> getGoodsByTypeId(Integer goodsTypeId);
}
