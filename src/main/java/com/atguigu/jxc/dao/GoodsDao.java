package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.User;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @description 商品信息
 */
public interface GoodsDao {

    //调用dao层进行新增
    void saveGoods(Goods goods);

    //通过goodsId 进行删除数据
    void deleteGoods(Integer goodsId);

    //调用dao层进行修改
    void updateGoods(Goods goods);

    String getMaxCode();

    /**
     * 分页查询商品库存信息
     * @param offSet 从第几条开始 到 ...
     * @param rows 每页显示条数
     * @param codeOrName 商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    List<Goods> getGoodsInventoryList(int offSet, Integer pageRow, String codeOrName, Integer goodsTypeId);

    // 查询类别ID为当前ID或父ID为当前类别ID的商品
    List<Goods> getGoodsList(int offSet, Integer pageRow, String goodsName, Integer goodsTypeId);

    //通过id或商品名称查询到记录数返回给前端
    Integer getGoodsCount(String goodsName, Integer goodsTypeId);

    //根据goodsId查回商品实体
    Goods findByGoodsId(Integer goodsId);

    //通过第几页开始，一共多少条 商品名称或编码查询 没有库存商品
    List<Goods> getNoInventoryQuantity(int offSet, Integer pageRow, String nameOrCode);

    //通过商品名称或编码查询到没有库存相关商品的总数量返回给前端 total
    Integer getNoInventoryQuantityCount(String nameOrCode);

    //通过从第几页开始，每页显示多少条，商品名称或商品编码进行有库存商品查询
    List<Goods> getHasInventoryQuantity(int offSet, Integer pageRow, String nameOrCode);

    //通过商品名称或编码查询到有库存相关商品的总数量返回给前端 total
    Integer getHasInventoryQuantityCount(String nameOrCode);

    //通过库存数量小于库存下限 判定为库存报警
    List<Goods> getGoodsAlarm();

    //根据商品编码或名称 或商品ID 进行查询得到一共多少条数据
    Integer getGoodsInventoryCount(String codeOrName, Integer goodsTypeId);
}
