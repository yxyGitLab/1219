package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.entity.Unit;

import java.util.List;

public interface UnitDao {

    //调用持久层查询所有商品单位
    List<Unit> listAll();

}
