package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/customer")
@RestController
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    /**
     * 添加或修改客户
     * @param customer 客户实体
     * @return
     */
    @PostMapping("/save")
    public ServiceVO save(Customer customer){
        //调用业务层添加或修改客户
        return customerService.save(customer);
    }

    /**
     * 删除客户 支持批量删除
     * @param ids
     * @return
     */
    @PostMapping("/delete")
    public ServiceVO delete(String ids){
        return customerService.delete(ids);
    }

    /**
     * 分页查询客户
     * @param page 当前页数
     * @param rows 每页显示的记录数
     * @param customerName 客户名
     * @return
     */
    @PostMapping("/list")
    public Map<String,Object> findAll(Integer page, Integer rows, String customerName){
        return customerService.list(page, rows, customerName);
    }

}
