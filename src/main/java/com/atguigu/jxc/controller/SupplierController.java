package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/supplier")
@RestController
public class SupplierController {
    @Autowired
    private SupplierService supplierService;

    /**
     * 添加或修改供应商
     * @param supplier 供应商实体
     * @return
     */
    @PostMapping("/save")
    public ServiceVO save(Supplier supplier){
        //调用业务层方法继续添加或修改
        return supplierService.save(supplier);
    }

    /**
     * 删除供应商
     * @param ids 供应商ids字符串，用逗号分隔 存入集合批量删除
     * @return
     */
    @PostMapping("/delete")
    public ServiceVO delete(@RequestParam String ids){
        return supplierService.delete(ids);
    }

    /**
     * 分页查询供应商管理列表
     * @param page 当前页数
     * @param rows 每页显示的记录数
     * @param supplierName 供应商名
     * @return
     */
    @PostMapping("/list")
    public Map<String,Object> list(Integer page, Integer rows, String supplierName){
        return supplierService.list(page,rows,supplierName);
    }

}
