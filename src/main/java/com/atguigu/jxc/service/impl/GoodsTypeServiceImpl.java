package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.GoodsTypeService;
import com.atguigu.jxc.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @description
 */
@Service
public class GoodsTypeServiceImpl implements GoodsTypeService {

    @Autowired
    private LogService logService;
    @Autowired
    private GoodsTypeDao goodsTypeDao;

    /**
     * 添加商品类别 新增分类
     * @param goodsTypeName 类别名
     * @param pId 父类ID
     * @return
     */
    @Override
    public ServiceVO save(String goodsTypeName, Integer pId) {
        //封装参数到实体 状态值为0必填 数据库设定非空
        GoodsType goodsType = new GoodsType(goodsTypeName,0,pId);
        //调用持久层进行新增
        goodsTypeDao.saveGoodsType(goodsType);
        // 根据父类ID来查询父类实体
        GoodsType parentGoodsType = goodsTypeDao.getGoodsTypeById(pId);
        // 如果当前父商品类别是叶子节点，则需要修改为状态为根节点
        if(parentGoodsType.getGoodsTypeState() == 0) {
            //修改节点值为1，即为根节点
            parentGoodsType.setGoodsTypeState(1);
            //状态为0表示其为叶子节点， 1为根节点，-1为顶级根节点即所有类别
            //修改状态值后进行父类节点状态修改
            goodsTypeDao.updateGoodsTypeState(parentGoodsType);
        }
        //保存日志
        logService.save(new Log(Log.INSERT_ACTION, "新增商品类别:"+goodsTypeName));
        //封装参数给前端，表示新增成功
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public  ArrayList<Object> loadGoodsType() {
        logService.save(new Log(Log.SELECT_ACTION, "查询商品类别信息"));


        return this.getAllGoodsType(-1); // 根节点默认从-1开始
    }



    /**
     * 删除分类
     * @param goodsTypeId
     */
    @Override
    public ServiceVO delete(Integer goodsTypeId) {
        // 根据商品类别ID来查询商品信息，如果该类别下有商品信息，则不给予删除
        List<Goods> goodsList = goodsTypeDao.getGoodsByTypeId(goodsTypeId);
        //如果查回来的商品数量不等于0，说明有商品
        if (goodsList.size() != 0) {
            //表示该商品类别下有商品，无法删除
            return new ServiceVO<>(ErrorCode.GOODS_TYPE_ERROR_CODE, ErrorCode.GOODS_TYPE_ERROR_MESS);
        }
        // 这里的逻辑是先根据商品类别ID查询出商品类别的信息，找到商品类别的父级商品类别
        // 如果父商品类别的子商品类别信息等于1，那么再删除商品信息的时候，父级商品类别的状态也应该从根节点改为叶子节点
        GoodsType goodsType = goodsTypeDao.getGoodsTypeById(goodsTypeId);
        //根据商品的父级id Pid 查询回相关所有pid集合的 商品类别集合 封装到goodTypeList集合中
        List<GoodsType> goodsTypeList = goodsTypeDao.getAllGoodsTypeByParentId(goodsType.getPId());
        //如果商品类型集合的数量等于1 ，那么再删除商品信息的时候，父级商品类别的状态也应该从根节点改为叶子节点
        if(goodsTypeList.size() == 1){
            //通过pid 查到这条商品类型的整体数据（实体对象）
            GoodsType parentGoodsType = goodsTypeDao.getGoodsTypeById(goodsType.getPId());
            //商品类型实体对象修改状态值为0，表示其为叶子节点
            parentGoodsType.setGoodsTypeState(0);
            //把修改好的商品类型对象传入dao层进行修改参数
            goodsTypeDao.updateGoodsTypeState(parentGoodsType);
        }
        //至此 根据传过来的 商品类别Id进行删除
        goodsTypeDao.delete(goodsTypeId);
        //记录保存日志
        logService.save(new Log(Log.DELETE_ACTION,"删除商品类别："+goodsType.getGoodsTypeName()));
        //封装相应参数给前端 表明删除成功
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);

    }


    /**
     * 递归查询所有商品类别
     *
     * @return
     */
    public ArrayList<Object> getAllGoodsType(Integer parentId){

        ArrayList<Object> array = this.getGoodSTypeByParentId(parentId);

        for(int i = 0;i < array.size();i++){

            HashMap obj = (HashMap) array.get(i);

            if(obj.get("state").equals("open")){// 如果是叶子节点，不再递归

            }else{// 如果是根节点，继续递归查询
                obj.put("children", this.getAllGoodsType(Integer.parseInt(obj.get("id").toString())));
            }

        }

        return array;
    }

    /**
     * 根据父ID获取所有子商品类别
     * @return
     */
    public ArrayList<Object> getGoodSTypeByParentId(Integer parentId){

        ArrayList<Object> array = new ArrayList<>();

        List<GoodsType> goodsTypeList = goodsTypeDao.getAllGoodsTypeByParentId(parentId);

        System.out.println("goodsTypeList" + goodsTypeList);
        //遍历商品类别
        for(GoodsType goodsType : goodsTypeList){

            HashMap obj = new HashMap<String, Object>();

            obj.put("id", goodsType.getGoodsTypeId());
            obj.put("text", goodsType.getGoodsTypeName());

            if(goodsType.getGoodsTypeState() == 1){
                obj.put("state", "closed");

            }else{
                obj.put("state", "open");
            }

            obj.put("iconCls", "goods-type");

            HashMap<String, Object> attributes = new HashMap<>();
            attributes.put("state", goodsType.getGoodsTypeState());
            obj.put("attributes", attributes);

            array.add(obj);

        }

        return array;
    }
}
