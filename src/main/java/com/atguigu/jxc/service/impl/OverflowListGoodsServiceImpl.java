package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.OverflowListGoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.OverflowListGoodsService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OverflowListGoodsServiceImpl implements OverflowListGoodsService {

    @Autowired
    private LogService logService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private GoodsDao goodsDao;

    @Autowired
    private OverflowListGoodsDao overflowListGoodsDao;

    /**
     * 保存商品报溢单信息
     * @param overflowList 商品报溢单信息实体
     * @param overflowListGoodsStr 商品报溢商品信息JSON字符串
     * @return
     */
    @Override
    public ServiceVO save(OverflowList overflowList, String overflowListGoodsStr) {
        // 使用谷歌Gson将JSON字符串数组转换成具体的集合
        Gson gson = new Gson();
        //将前端传过来的json数据转为实体
        List<OverflowListGoods> overflowListGoodsList =
                gson.fromJson(overflowListGoodsStr,new TypeToken<List<OverflowListGoods>>(){}.getType());

        // 设置当前操作用户
        User currentUser = userDao.findUserByName((String) SecurityUtils.getSubject().getPrincipal());

        overflowList.setUserId(currentUser.getUserId());

        // 保存商品报溢单信息
        overflowListGoodsDao.saveOverflowList(overflowList);

        // 保存商品报溢单商品信息
        for(OverflowListGoods overflowListGoods : overflowListGoodsList){
            //循环遍历商品报溢单实体到 商品报溢单商品列表
            overflowListGoods.setOverflowListId(overflowList.getOverflowListId());
            //调用dao层保存   商品报溢单商品列表
            overflowListGoodsDao.saveOverflowListGoods(overflowListGoods);

            // 修改商品库存，状态
            Goods goods = goodsDao.findByGoodsId(overflowListGoods.getGoodsId());
            //修改库存数量
            goods.setInventoryQuantity(goods.getInventoryQuantity()+overflowListGoods.getGoodsNum());
            //修改2为 有进货或销售单据
            goods.setState(2);
            //调用dao层进行商品信息修改
            goodsDao.updateGoods(goods);

        }

        // 保存日志
        logService.save(new Log(Log.INSERT_ACTION, "新增商品报溢单："+overflowList.getOverflowNumber()));
        //返回相关参数给前端，表示新增报溢单成功
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    /**
     * 查询商品报溢单
     * @param sTime 开始时间
     * @param eTime 结束时间
     * @return
     */
    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        Map<String,Object> result = new HashMap<>();

        try {
            //根据开始时间和结束时间 查询商品报溢单
            List<OverflowList> overflowListList = overflowListGoodsDao.getOverflowlist(sTime, eTime);
            //保存日志
            logService.save(new Log(Log.SELECT_ACTION, "商品报溢单据查询"));
            //返回数据给前端渲染
            result.put("rows", overflowListList);

        } catch (Exception e) {

            e.printStackTrace();

        }
        //返回数据给控制层
        return result;
    }

    /**
     * 查询商品报溢单商品信息
     * @param overflowListId 商品报溢单ID
     * @return
     */
    @Override
    public Map<String, Object> goodsList(Integer overflowListId) {
        Map<String,Object> map = new HashMap<>();

        try {
            //根据商品报溢单ID查询商品报溢单商品信息
            List<OverflowListGoods> overflowListGoodsList = overflowListGoodsDao.getOverflowListGoodsByOverflowListId(overflowListId);
            //保存日志
            logService.save(new Log(Log.SELECT_ACTION, "商品报溢单商品信息查询"));
            //返回rows数据给前端渲染
            map.put("rows", overflowListGoodsList);

        } catch (Exception e) {

            e.printStackTrace();

        }
        //返回数据给控制层
        return map;
    }
}
