package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierDao supplierDao;

    @Autowired
    private LogService logService;

    //调用业务层方法继续添加或修改
    @Override
    public ServiceVO save(Supplier supplier) {
        //如果供应商ID为空，说明是新增
        if(supplier.getSupplierId()==null){
            //调用持久层进行新增
            supplierDao.saveSupplier(supplier);
            //进行日志保存
            logService.save(new Log(Log.INSERT_ACTION,"添加供应商:"+supplier.getSupplierName()));
        }else {
            //如果供应商ID不为空 ，则是修改
            supplierDao.updateSupplier(supplier);
            logService.save(new Log(Log.UPDATE_ACTION,"修改供应商:"+supplier.getSupplierName()));
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }


    /**
     * 删除供应商
     * @param ids
     */
    @Override
    public ServiceVO delete(String ids) {
        //拆分传过来的数组
        String[] idArray = ids.split(",");
        //遍历数组取到  前台传过来的数组中的每个id
        for (String id : idArray) {
            //通过getSupplierById方法传入supplierId得到供应商名称写入日志
            logService.save(new Log(Log.DELETE_ACTION,
                    "删除供应商:" + supplierDao.getSupplierById(Integer.parseInt(id)).getSupplierName()));
            //调用持久层进行循环删除
            supplierDao.deleteSupplier(Integer.parseInt(id));
        }
        //封装参数返回给前端 表示删除成功
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }


    /**
     * 分页查询供应商管理列表
     * @param page 当前页数
     * @param rows 每页显示的记录数
     * @param supplierName 供应商名
     * @return
     */
    @Override
    public Map<String, Object> list(Integer page, Integer rows, String supplierName) {
        Map<String,Object> map = new HashMap<>();
        //做分页
        page=page==0?1:page;
        int offSet=(page-1)*rows;
        //调用持久层查询所有的供应商管理列表
        List<Supplier> supplierList=supplierDao.getSupplierList(offSet,rows,supplierName);
        //操作保存日志
        logService.save(new Log(Log.SELECT_ACTION,"分页查询供应商"));
        //查询出 查到数据的总记录数 一共多少条 前端要求返回的 total
        map.put("total",supplierDao.getSupplierCount(supplierName));
        //rows 前端要求返回的集合名称
        map.put("rows",supplierList);
        return map;
    }

}
