package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.DamageListGoodsService;
import com.atguigu.jxc.service.LogService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DamageListGoodsServiceImpl implements DamageListGoodsService {

    @Autowired
    private LogService logService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private DamageListGoodsDao damageListGoodsDao;

    @Autowired
    private GoodsDao goodsDao;

    /**
     * 保存商品报损单信息
     * @param damageList 商品报损单信息实体
     * @param damageListGoodsStr 商品报损商品信息JSON字符串
     * @return
     */
    @Override
    public ServiceVO save(DamageList damageList, String damageListGoodsStr) {

        // 使用谷歌Gson将JSON字符串数组转换成具体的集合
        Gson gson = new Gson();

//该方法将指定的Json反序列化为指定类型的对象。如果指定的对象是泛型类型，此方法很有用。
// 对于非泛型对象，使用fromJson(String, Class)代替。
// 如果你在Reader中有Json而不是字符串，使用fromJson(Reader, Type)代替。如果JSON字符串有多个顶级JSON元素，或者有尾随数据，则抛出异常。
// Params: json -对象被反序列化的字符串类型- src的特定泛化类型。您可以通过使用TypeToken类获得此类型。
// 例如，要获取Collection<Foo>的类型，您应该使用:type typeOfT = new TypeToken<Collection<Foo>>(){}.getType();
// 从字符串返回一个T类型的对象。如果json为空或json为空则返回null。
// 抛出:JsonParseException -如果json不是一个类型为typeft的对象的有效表示JsonSyntaxException -如果json不是一个类型的对象的有效表示
        List<DamageListGoods> damageListGoodsList = gson.fromJson(damageListGoodsStr,new TypeToken<List<DamageListGoods>>(){}.getType());

        // 设置当前操作用户  根据运行时环境访问当前可访问的调用代码的Subject。
        //此方法是作为一种获取Subject的方法而提供的，而不必诉诸于特定于实现的方法。
        // 它还允许Shiro团队在未来根据需求更新更改此方法的底层实现，而不会影响使用它的代码。
        // 返回:调用代码可访问的当前可访问的Subject。
        // 抛出:IllegalStateException——如果没有Subject实例或SecurityManager实例可用来获取一个Subject，
        // 这被认为是无效的应用程序配置——一个Subject应该始终对调用者可用。
        //返回这个Subject的应用程序范围内唯一标识主体，如果这个Subject是匿名的，
        // 因为它还没有任何相关的帐户数据(例如，如果他们还没有登录)，则返回null。
        User currentUser = userDao.findUserByName((String) SecurityUtils.getSubject().getPrincipal());
        //为当前报损单实体设置
        damageList.setUserId(currentUser.getUserId());

        // 保存商品报损单信息
        damageListGoodsDao.saveDamageList(damageList);

        // 保存商品报损单商品信息
        for(DamageListGoods damageListGoods : damageListGoodsList){
            //遍历集合保存商品各项信息
            damageListGoods.setDamageListId(damageList.getDamageListId());
            //保存报损单信息到商品报损单商品列表
            damageListGoodsDao.saveDamageListGoods(damageListGoods);

            // 修改商品库存，状态  根据商品id查询 商品实体
            Goods goods = goodsDao.findByGoodsId(damageListGoods.getGoodsId());
            //修改库存数量
            goods.setInventoryQuantity(goods.getInventoryQuantity()-damageListGoods.getGoodsNum());
            //修改2为 有进货或销售单据
            goods.setState(2);
            //调用业务层进行修改
            goodsDao.updateGoods(goods);

        }

        // 保存日志
        logService.save(new Log(Log.INSERT_ACTION, "新增商品报损单："+damageList.getDamageNumber()));
        //返回相关参数给前端，表示保存商品报损单信息成功
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    /**
     * 查询商品报损单
     * @param sTime 开始时间
     * @param eTime 结束时间
     * @return
     */
    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        Map<String,Object> result = new HashMap<>();

        try {
            //通过开始时间和结束时间到dao层查询 商品报损单
            List<DamageList> damageListList = damageListGoodsDao.getDamagelist(sTime, eTime);
            //保存日志
            logService.save(new Log(Log.SELECT_ACTION, "商品报损单据查询"));
            //返回 rows数据给前端
            result.put("rows", damageListList);

        } catch (Exception e) {

            e.printStackTrace();

        }
        //返回结果到控制层
        return result;
    }

    /**
     * 查询商品报损单商品信息
     * @param damageListId 商品报损单ID
     * @return
     */
    @Override
    public Map<String, Object> goodsList(Integer damageListId) {
        Map<String,Object> map = new HashMap<>();

        try {
            //根据 商品保存单ID 查询商品报损单商品信息
            List<DamageListGoods> damageListGoodsList = damageListGoodsDao.getDamageListGoodsByDamageListId(damageListId);
            //保存日志
            logService.save(new Log(Log.SELECT_ACTION, "商品报损单商品信息查询"));
            //返回rows数据供前端渲染
            map.put("rows", damageListGoodsList);

        } catch (Exception e) {

            e.printStackTrace();

        }
        //返回给控制层
        return map;
    }

}
