package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.GoodsTypeDao;

import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;

import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;


import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private LogService logService;

    @Autowired
    private GoodsDao goodsDao;

    @Autowired
    private SaleListGoodsService saleListGoodsService;

    @Autowired
    private CustomerReturnListGoodsService customerReturnListGoodsService;

    /**
     * 添加或修改商品信息
     * @param goods 商品信息实体
     * @return
     */
    @Override
    public ServiceVO save(Goods goods) {
        //如果GoodsId为空，说明是新增
        if(goods.getGoodsId() == null){
            //记录日志
            logService.save(new Log(Log.INSERT_ACTION,"添加商品:"+goods.getGoodsName()));
            // 设置上一次采购价为当前采购价
            goods.setLastPurchasingPrice(goods.getPurchasingPrice());
            //数据库要求非空
            goods.setInventoryQuantity(0);
            //数据库要求非空
            goods.setState(0);
            //调用dao层进行新增
            goodsDao.saveGoods(goods);

        }else{
            //如果GoodsId不为空，说明是修改
            //调用dao层进行修改
            goodsDao.updateGoods(goods);
            logService.save(new Log(Log.UPDATE_ACTION,"修改商品:"+goods.getGoodsName()));

        }
        //返回相关参数给前端 表示新增或修改成功
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    /**
     * 添加商品期初库存
     * @param goodsId 商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice 成本价
     * @return
     */
    @Override
    public ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        //根据商品Id查询回商品实体
        Goods goods = goodsDao.findByGoodsId(goodsId);
        //给商品实体设置 从前端传过来的inventoryQuantity 库存
        goods.setInventoryQuantity(inventoryQuantity);
        //给商品实体设置 从前端传过来的purchasingPrice 成本价 采购价格
        goods.setPurchasingPrice(purchasingPrice);
        //给商品实体设置 从前端传过来的purchasingPrice 上一次成本价
        goods.setLastPurchasingPrice(purchasingPrice);
        //调用dao层进行修改商品
        goodsDao.updateGoods(goods);
        //保存日志
        logService.save(new Log(Log.UPDATE_ACTION,goods.getGoodsName()+"商品期初入库"));
        //返回相关参数给前端，表示添加，修改库存，采购价格成功
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    /**
     * 删除商品信息
     * @param goodsId 商品ID
     * @return
     */
    @Override
    public ServiceVO delete(Integer goodsId) {
        //根据goodsId查回商品实体
        Goods goods = goodsDao.findByGoodsId(goodsId);
        //如果得到的state为1 表明已入库不能删除
        if (goods.getState() == 1) {
            // 返回 该商品已入库，不能删除
            return new ServiceVO<>(ErrorCode.STORED_ERROR_CODE, ErrorCode.STORED_ERROR_MESS);
        //如果得到的state为1 表明有进货或销售单据 接口要求不能删除
        } else if (goods.getState() == 2) {
            //返回 该商品有进货或销售单据，不能删除
            return new ServiceVO<>(ErrorCode.HAS_FORM_ERROR_CODE, ErrorCode.HAS_FORM_ERROR_MESS);

        } else {
            //记录日志
            logService.save(new Log(Log.DELETE_ACTION,"删除商品:"+goods.getGoodsName()));
            //其他情况可以调用dao层进行删除
            //通过goodsId 进行删除
            goodsDao.deleteGoods(goodsId);

        }
        //返回相关参数给前端表示删除商品成功
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for(int i = 4;i > intCode.toString().length();i--){

            unitCode = "0"+unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    /**
     * 分页查询商品库存信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param codeOrName 商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @Override
    public Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        Map<String,Object> map=new HashMap<>();

        //做分页
        //当前页
        page=page==0?1:page;
        //每页显示数量 如果传入1，就是从0到rows条
        int offSet=(page-1)*rows;
        //调用持久层查询数据
        List<Goods> goodsList = goodsDao.getGoodsInventoryList(offSet, rows, codeOrName, goodsTypeId);

   /*     for (Goods goods : goodsList) {
            // 销售总量等于销售单据的销售数据减去退货单据的退货数据
            goods.setSaleTotal(saleListGoodsService.getSaleTotalByGoodsId(goods.getGoodsId())
                    - customerReturnListGoodsService.getCustomerReturnTotalByGoodsId(goods.getGoodsId()));

        }*/
        //返回数据给前端渲染
        map.put("rows", goodsList);
        //根据商品编码或名称 或商品ID 进行查询得到一共多少条数据
        map.put("total", goodsDao.getGoodsInventoryCount(codeOrName, goodsTypeId));
        //保存日志
        logService.save(new Log(Log.SELECT_ACTION, "分页查询商品库存信息"));

        return map;
    }

    /**
     * 分页查询商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param goodsName 商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @Override
    public Map<String, Object> list(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        Map<String,Object> map = new HashMap<>();
        //做分页
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        // 查询类别ID为当前ID或父ID为当前类别ID的商品
        List<Goods> goodsList = goodsDao.getGoodsList(offSet, rows, goodsName, goodsTypeId);
        //传递每页多少条给前端
        map.put("rows", goodsList);
        //通过id或商品名称查询到记录数返回给前端
        map.put("total", goodsDao.getGoodsCount(goodsName, goodsTypeId));
        //保存日志
        logService.save(new Log(Log.SELECT_ACTION, "分页查询商品信息"));
        //封装参数返回给控制层
        return map;
    }

    /**
     * 分页查询无库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String,Object> map = new HashMap<>();
        //做分页
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        //通过第几页开始，一共多少条 商品名称或编码查询 没有库存商品
        List<Goods> goodsList = goodsDao.getNoInventoryQuantity(offSet, rows, nameOrCode);
        //传递前端规定参数rows 到前端
        map.put("rows", goodsList);
        //通过商品名称或编码查询到没有库存相关商品的总数量返回给前端 total
        map.put("total", goodsDao.getNoInventoryQuantityCount(nameOrCode));
        //保存日志
        logService.save(new Log(Log.SELECT_ACTION, "分页查询商品信息（无库存）"));
        //返回map集合给控制层
        return map;
    }

    /**
     * 分页查询有库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String,Object> map = new HashMap<>();
        //做分页
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        //通过从第几页开始，每页显示多少条，商品名称或商品编码进行有库存商品查询
        List<Goods> goodsList = goodsDao.getHasInventoryQuantity(offSet, rows, nameOrCode);
        //将查询到的有库存集合存入map
        map.put("rows", goodsList);
        //通过商品名称或编码查询到有库存相关商品的总数量返回给前端 total
        map.put("total", goodsDao.getHasInventoryQuantityCount(nameOrCode));
        //保存日志
        logService.save(new Log(Log.SELECT_ACTION, "分页查询商品信息（有库存）"));
        //返回集合给控制层
        return map;
    }

    /**
     * 查询库存报警商品信息
     * @return
     */
    @Override
    public Map<String, Object> listAlarm() {
        Map<String,Object> map = new HashMap<>();
        //通过库存数量小于库存下限 判定为库存报警
        //返回当前库存量 小于 库存下限的商品信息
        List<Goods> goodsList = goodsDao.getGoodsAlarm();
        //rows为前端所需数据，按要求返回
        map.put("rows", goodsList);
        //保存日志
        logService.save(new Log(Log.SELECT_ACTION, "查询库存报警商品信息"));
        //返回集合到控制层
        return map;
    }
}
