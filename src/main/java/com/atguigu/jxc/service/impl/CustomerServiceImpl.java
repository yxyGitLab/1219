package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;


import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.CustomerService;
import com.atguigu.jxc.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private LogService logService;

    /**
     * 添加或修改客户
     * @param customer 客户实体
     * @return
     */
    @Override
    public ServiceVO save(Customer customer) {
        if(customer.getCustomerId() == null){
            //如果客户id为空说明是新增
            customerDao.saveCustomer(customer);
            logService.save(new Log(Log.INSERT_ACTION,"添加客户:"+customer.getCustomerName()));

        }else{
            //如果客户id不为空说明是修改
            customerDao.updateCustomer(customer);
            logService.save(new Log(Log.UPDATE_ACTION,"修改客户:"+customer.getCustomerName()));

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    /**
     * 进行删除客户（支持批量删除）
     * @param ids
     * @return
     */
    @Override
    public ServiceVO delete(String ids) {
        //拆分前端传过来的ids
        String[] idArray = ids.split(",");
        //遍历数组中的元素，循环一次删除一个（ids中的）客户
        for(String id : idArray){
            //通过CustomerById 查询出相关id客户的名称
            logService.save(new Log(Log.DELETE_ACTION,
                    "删除客户:" + customerDao.getCustomerById(Integer.parseInt(id)).getCustomerName()));
            //调用持久层 按遍历的id进行 循环删除客户
            customerDao.deleteCustomer(Integer.parseInt(id));
        }
        //封装参数返回给前端删除成功
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    /**
     * 分页查询客户
     * @param page 当前页数
     * @param rows 每页显示的记录数
     * @param customerName 客户名
     * @return
     */
    @Override
    public Map<String, Object> list(Integer page, Integer rows, String customerName) {
        Map<String,Object> map = new HashMap<>();
        //做分页
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        //通过前端传过来的名称进行模糊查询
        List<Customer> customers = customerDao.getCustomerList(offSet, rows, customerName);

        logService.save(new Log(Log.SELECT_ACTION,"分页查询客户"));
        //根据客户名称模糊查询出一共多少条记录 total是前端要求返回的值
        map.put("total", customerDao.getCustomerCount(customerName));
        //rows是前端要求返回的参数
        map.put("rows", customers);
        //封装在map集合中返回
        return map;
    }

}
