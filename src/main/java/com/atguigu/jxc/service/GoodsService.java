package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import io.swagger.models.auth.In;

import java.util.List;
import java.util.Map;

public interface GoodsService  {

    /**
     * 添加或修改商品信息
     * @param goods 商品信息实体
     * @return
     */
    ServiceVO save(Goods goods);

    /**
     * 添加商品期初库存
     * @param goodsId 商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice 成本价
     * @return
     */
    ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);

    //通过goodsId进行删除
    ServiceVO delete(Integer goodsId);

    ServiceVO getCode();

    /**
     * 分页查询商品库存信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param codeOrName 商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);

    /**
     * 分页查询商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param goodsName 商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    Map<String, Object> list(Integer page, Integer rows, String goodsName, Integer goodsTypeId);

    /**
     * 分页查询无库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    /**
     * 分页查询有库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    /**
     * 查询库存报警商品信息
     * @return
     */
    Map<String, Object> listAlarm();

}
