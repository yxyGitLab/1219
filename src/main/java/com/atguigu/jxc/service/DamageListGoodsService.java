package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;

import java.util.Map;

public interface DamageListGoodsService {

    /**
     * 保存商品报损单信息
     * @param damageList 商品报损单信息实体
     * @param damageListGoodsStr 商品报损商品信息JSON字符串
     * @return
     */
    ServiceVO save(DamageList damageList, String damageListGoodsStr);

    /**
     * 查询商品报损单
     * @param sTime 开始时间
     * @param eTime 结束时间
     * @return
     */
    Map<String, Object> list(String sTime, String eTime);

    /**
     * 查询商品报损单商品信息
     * @param damageListId 商品报损单ID
     * @return
     */
    Map<String, Object> goodsList(Integer damageListId);
}
