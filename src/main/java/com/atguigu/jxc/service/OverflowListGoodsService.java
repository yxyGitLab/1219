package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;

import java.util.Map;

public interface OverflowListGoodsService {

    /**
     * 保存商品报溢单信息
     * @param overflowList 商品报溢单信息实体
     * @param overflowListGoodsStr 商品报溢商品信息JSON字符串
     * @return
     */
    ServiceVO save(OverflowList overflowList, String overflowListGoodsStr);

    /**
     * 查询商品报溢单
     * @param sTime 开始时间
     * @param eTime 结束时间
     * @return
     */
    Map<String, Object> list(String sTime, String eTime);

    /**
     * 查询商品报溢单商品信息
     * @param overflowListId 商品报溢单ID
     * @return
     */
    Map<String, Object> goodsList(Integer overflowListId);
}
