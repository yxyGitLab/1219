package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;

import java.util.ArrayList;

/**
 * @description
 */
public interface GoodsTypeService {

    /**
     * 添加商品类别 新增分类
     * @param goodsTypeName 类别名
     * @param pId 父类ID
     * @return
     */
    ServiceVO save(String goodsTypeName, Integer pId);

    /**
     * 删除分类
     * @param goodsTypeId
     */
    ServiceVO delete(Integer goodsTypeId);

    ArrayList<Object> loadGoodsType();






}
