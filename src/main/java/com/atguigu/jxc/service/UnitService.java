package com.atguigu.jxc.service;


import com.atguigu.jxc.entity.Unit;

import java.util.List;
import java.util.Map;

public interface UnitService {

    /**
     * 查询所有商品单位
     * @return
     */
    Map<String, Object> list();
}
