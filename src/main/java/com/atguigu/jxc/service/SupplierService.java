package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;

import java.util.List;
import java.util.Map;

public interface SupplierService {

    //调用业务层方法继续添加或修改
    ServiceVO save(Supplier supplier);

    /**
     * 删除供应商
     * @param ids
     */
    ServiceVO delete(String ids);

    /**
     * 分页查询供应商管理列表
     * @param page 当前页数
     * @param rows 每页显示的记录数
     * @param supplierName 供应商名
     * @return
     */
    Map<String, Object> list(Integer page, Integer rows, String supplierName);

}
