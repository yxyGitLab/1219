package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;

import java.util.List;
import java.util.Map;

public interface CustomerService {

    //调用业务层添加或修改客户
    ServiceVO save(Customer customer);

    /**
     * 删除客户
     * @param ids
     */
    ServiceVO delete(String ids);

    /**
     * 分页查询客户
     * @param page 当前页数
     * @param rows 每页显示的记录数
     * @param customerName 客户名
     * @return
     */
    Map<String, Object> list(Integer page, Integer rows, String customerName);

}
